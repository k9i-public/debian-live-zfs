#!/bin/sh
set -eu

mkdir -pv config/includes.chroot/root/.ssh

# for ssh
if [ -f ~/.ssh/authorized_keys ]; then
  cp -pv ~/.ssh/authorized_keys config/includes.chroot/root/.ssh/
fi

# for screen
if [ -f ~/.screenrc ]; then
  cp -pv ~/.screenrc config/includes.chroot/root/.screenrc
elif [ -f ~/.dot/screen/screenrc ]; then
  cp -pv ~/.dot/screen/screenrc config/includes.chroot/root/.screenrc
fi
