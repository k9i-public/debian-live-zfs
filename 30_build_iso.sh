#!/bin/sh
set -eu
( set -x; lb bootstrap && lb chroot && lb binary && ls -laF *.iso )
